package lab2;

import cse131.ArgsProcessor;

/* 
 * one plays randomly 
 * while the other rotates faithfully from rock to paper to scissors
 * simulate this game and to report the fraction of games won by each of the two players.
 */
public class RPS {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); // input popup
		int numGames = ap.nextInt("How many times would you like to play the game?"); // read
																						// input

		// initialize values
		double randChoice = 0;
		double twoPlayer = 0;
		double onePlayer = 0;
		double onepWins = 0;
		double twopWins = 0;
		double tiedGames = 0;

		// loop it up
		for (int i = 0; i < numGames; i++) {
			onePlayer = Math.random();
			// oneplayer
			if (onePlayer < 0.33) {
				randChoice = 1; // rock
			} else if (onePlayer < 0.67) {
				randChoice = 2; // paper
			} else {
				randChoice = 3; // scissors
			}

			/*
			 * twoplayer if (cyclePlayer == 0) { twoPlayer = 1; // rock
			 * cyclePlayer = cyclePlayer + 1; } else if (cyclePlayer == 1) {
			 * twoPlayer = 2; // paper cyclePlayer = cyclePlayer + 1; } else if
			 * (cyclePlayer == 2) { twoPlayer = 3; // scissors cyclePlayer = 0;
			 * }
			 */
			// player two
			if (i % 3 == 0) {
				twoPlayer = 1; // rock
			} else if (i % 3 == 1) {
				twoPlayer = 2; // paper
			} else {
				twoPlayer = 3; // scissors
			}

			// compare
			if (randChoice == 1) { // rock
				if (twoPlayer == 1) { // rock v rock
					tiedGames++;
				} else if (twoPlayer == 2) { // rock v paper
					twopWins += 1;
				} else if (twoPlayer == 3) { // rock v scissors
					onepWins += 1;
				}
			}

			if (randChoice == 2) { // paper
				if (twoPlayer == 1) { // paper v rock
					onepWins += 1;
				} else if (twoPlayer == 2) { // paper v paper
					tiedGames++;
				} else if (twoPlayer == 3) { // paper v scissors
					twopWins += 1;
				}
			}

			if (randChoice == 3) { // scissors
				if (twoPlayer == 1) { // scissors v rock
					twopWins += 1;
				} else if (twoPlayer == 2) { // scissors v paper
					onepWins += 1;
				} else if (twoPlayer == 3) { // scissors v scissors
					tiedGames++;
				}

			}
		}
		// make into fractions
		onepWins = (onepWins / numGames) * 100;
		twopWins = (twopWins / numGames) * 100;
		tiedGames = (tiedGames / numGames) * 100;

		// print it
		System.out.print("The random player won " + onepWins + " percent out of " + numGames + " games. "
				+ "The cycling player won " + twopWins + " percent out of " + numGames + " games. With " + tiedGames
				+ " percent games ending in a tie.");
	}
}
