package lab5;

public class Lab5Methods {

	public static int sumDownBy2(int n) {
		// the sum of the positive integers n + (n-2) + (n-4) + �
		if (n == 0) {
			return 0;
		}
		if (n == 1) {
			return 1;
		} else {
			return n + sumDownBy2(n - 2);
		}
	}

	public static double harmonicSum(double n) {
		// the sum 1 + 1/2 + 1/3 + � + 1/(n-1) + 1/n
		if (n == 1) {
			return 1;
		} else {
			return (1 / n) + harmonicSum(n - 1);
		}
	}

	public static double geometricSum(double k) {
		// 1 + 1/2 + 1/4 + 1/8 + � + 1/Math.pow(2,k)
		double counter = 0;
		for (double i = 0; i <= k; i++) {
			counter = counter + (1 / Math.pow(2, i));
		}
		return counter;
	}

	public static int multPos(int j, int k) {
		// product of j*k
		if (j == 0)
			return 0;
		int mult = k;
		for (int ii = 1; ii < Math.abs(j); ++ii) {
			mult += k;
		}
		return j >= 0 ? mult : -mult;
	}

	public static int mult(int j, int k) {
		// call mult pos pass the absolute values of j and k, +values only
		return multPos(j, k);
	}

	public static int expt(int n, int k) {
		// returns the value of n to the power of k
		if (k == 0) {
			return 1;
		} else if (k == 1) {
			return n;
		} else if (k > 1) {
			return (expt(n, k - 1) * n);
		} else {
			return expt(n, k + 1) / n;
		}

	}
}
