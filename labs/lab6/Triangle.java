package lab6;

import sedgewick.StdDraw;

public class Triangle {

	public static void tri(int n) {
		method(n, 0, 0, 1);
	}

	public static void method(int n, double x, double y, double size) {

		if (n == 0)
			return;

		// compute triangle points
		double x0 = x;
		double y0 = y;
		double x1 = x0 + size;
		double y1 = y0;
		double x2 = x0 + size / 2;
		double y2 = y0 + (Math.sqrt(3)) * size / 2;

		// draw the triangle
		// try filled shit instead of so many lines
		// StdDraw.FilledTriangle
		// StdDraw.filledPolygon(x0, y0);
		StdDraw.line(x0, y0, x1, y1);
		StdDraw.line(x0, y0, x2, y2);
		StdDraw.line(x1, y1, x2, y2);
		StdDraw.show(100);

		// recursive calls
		method(n - 1, x0, y0, size / 2);
		method(n - 1, (x0 + x1) / 2, (y0 + y1) / 2, size / 2);
		method(n - 1, (x0 + x2) / 2, (y0 + y2) / 2, size / 2);
	}

	public static void main(String[] args) {
		StdDraw.setPenRadius(0.005);
		tri(7);
	}
}