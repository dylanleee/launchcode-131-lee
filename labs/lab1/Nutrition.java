/*NOT THE REAL ONE

  package lab1;
 

import java.text.DecimalFormat;
import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); //input popup
		//decimal formation
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		//get initial values
		String name = ap.nextString("What is the name of the project you wish to analyze?");
		double carbs = ap.nextDouble("How many gram sof carbohydrates are in this product?");
		double fat = ap.nextDouble("How many grams of fat are in this food?");
		double pro = ap.nextDouble("How many grams of protien are in this food?");
		double statedCals = ap.nextDouble("How many caloriesare stated on this food's label?");
		
		//math
		carbs = (carbs*4);
		fat = (fat*9);
		pro = (pro*4);
		
		//percentage 
		double pCarbs = (carbs/statedCals)*100;
		double pFat = (fat/statedCals)*100;
		double pPro = (pro/statedCals)*100;
		
		double actualCals = (carbs+fat+pro);
		double unCals = (actualCals-statedCals);
		double fiber =(unCals/4);
		
		//boolean?
		boolean bCarbs = (pCarbs <= 25);
		boolean bFat = (pFat <= 15);
		boolean coin = (Math.random() >= .5);
		
		//final print statements
		System.out.println("The food is said to have "+statedCals+" (available) Calories.");
		System.out.println("With "+df.format(unCals)+" unavailable Calories, this food has "+df.format(fiber)+" grams of fiber");
		System.out.println(""); //empty line
		System.out.println("\t"+df.format(pCarbs)+"% of your food is carbohydrates.");
		System.out.println("\t"+df.format(pFat)+"% of your food is fat.");
		System.out.println("\t"+df.format(pPro)+"% of your food is protien.");
		System.out.println(""); //empty line
		System.out.println("Is this food acceptable for a low-carb diet? " +bCarbs);
		System.out.println("Is this food accaptable for a low-fat diet? " +bFat);
		System.out.println("By a coin flip should you try this food? " +coin);
		
	}

}
*/