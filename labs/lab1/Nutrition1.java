//actual right code
package lab1;

import java.text.DecimalFormat;

import cse131.ArgsProcessor;

public class Nutrition1 {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); // input popup
		// decimal formation
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		// get initial values
		String name = ap.nextString("What is the name of the project you wish to analyze?");
		double carbs = ap.nextDouble("How many gram of carbohydrates are in this product?");
		double fat = ap.nextDouble("How many grams of fat are in this food?");
		double pro = ap.nextDouble("How many grams of protien are in this food?");
		double statedCals = ap.nextDouble("How many caloriesare stated on this food's label?");

		// math
		carbs = (carbs * 4);
		fat = (fat * 9);
		pro = (pro * 4);

		// percentage
		double pCarbs = (carbs / statedCals) * 100;
		pCarbs = pCarbs * 10;
		pCarbs = Math.round(pCarbs);
		pCarbs = pCarbs / 10;

		double pFat = (fat / statedCals) * 100;
		pFat = pFat * 10;
		pFat = Math.round(pFat);
		pFat = pFat / 10;

		double pPro = (pro / statedCals) * 100;
		pPro = pPro * 10;
		pPro = Math.round(pPro);
		pPro = pPro / 10;

		double actualCals = (carbs + fat + pro);

		double unCals = (actualCals - statedCals);
		unCals = unCals * 10;
		unCals = Math.round(unCals);
		unCals = unCals / 10;

		double fiber = (unCals / 4);
		fiber = fiber * 10;
		fiber = Math.round(fiber);
		fiber = fiber / 10;

		// boolean?
		boolean bCarbs = (pCarbs <= 25);
		boolean bFat = (pFat <= 15);
		boolean coin = (Math.random() >= .5);

		// final print statements
		System.out.println("The food is said to have " + statedCals + " (available) Calories.");
		System.out.println("With " + unCals + " unavailable Calories, this food has " + fiber + " grams of fiber");
		System.out.println(""); // empty line
		System.out.println("\t" + pCarbs + "% of your food is carbohydrates.");
		System.out.println("\t" + pFat + "% of your food is fat.");
		System.out.println("\t" + pPro + "% of your food is protien.");
		System.out.println(""); // empty line
		System.out.println("Is this food acceptable for a low-carb diet? " + bCarbs);
		System.out.println("Is this food accaptable for a low-fat diet? " + bFat);
		System.out.println("By a coin flip should you try this food? " + coin);

	}

}
