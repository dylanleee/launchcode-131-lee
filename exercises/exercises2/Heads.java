package exercises2;

import cse131.ArgsProcessor;

public class Heads {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); //input popup
		int n = ap.nextInt("How many times would you like to run this program?"); //prompt user
		//the sum of all flip[s required to get 10 head per simulation
		int sumNumFlips =0;
		//beginning of my loop
		for(int i=0; i<n; i++){
			// what are the concepts?
			int numHeads = 0;
			int numFlips = 0;

			// now flip a coin until we see 10 heads
			while (numHeads != 10) {
				boolean isHeads = Math.random() < 0.5;       //random coin flip
				if (isHeads) {                               //if is head true?
					numHeads++;                 //hnumHeads++; would have worked here as well
				}
				numFlips++;

			}
			// here, numHeads should be 10
			sumNumFlips = sumNumFlips + numFlips;
			

		}//end of forloop
		System.out.println("Average number of flips was " + (1.0 * sumNumFlips/n));
	}

}
