package exercises2;

import cse131.ArgsProcessor;

public class BadSwap {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int x = ap.nextInt("Enter x");
		int y = ap.nextInt("Enter y");
		
		if (x > y) {
			int t=y; //save the initial value
			
			System.out.println("before"+y);
			y = x;     
			System.out.println("after"+y);
			x = t;    
		}
		
		System.out.println("x and y are now "
				+ x + " and " + y);
		
		//
		// Student fill in with comments below this line:
		//   Why does the code above *not* swap the values of
		//      x and y?
		//   Note that the bad swap executes only if x>y
		//
		// Your answer:
		//		There's no way to get back the initial 
		//        value of x after its first swap. need to make
		//			third value to hold value temporarily.
		//
		//
		//
		//

	}

}
