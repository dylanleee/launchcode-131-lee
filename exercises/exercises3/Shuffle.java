package exercises3;

public class Shuffle {

	public static void main(String[] args) {
		String[] original = { "A", "B", "C", "D", "E", "F", "G", "H" };

		// print original array
		for (int i = 0; i < original.length; ++i) {
			System.out.println("Original at " + i + " is " + original[i]);
		}
		// declare shuffled array, same size as original
		// String[] shuffled = new String[original.length];

		// iterate backwards over shuffled to fill in with
		for (int i = original.length - 1; i >= 0; i--) {
			int c = (int) (Math.random() * (i + 1));
			// shuffled[i] = original[c];
			// move card c to the "end" of the original deck
			// the "end" is actually index i
			// swapped original[i] with original[c]
			String t = original[i];
			original[i] = original[c];
			original[c] = t;

		}
		System.out.println("");
		// for (int i = 0; i < shuffled.length; ++i) {
		// System.out.println("Shuffled at " + i + " is " + shuffled[i]);
		// }
		System.out.println("");
		for (int i = 0; i < original.length; ++i) {
			System.out.println("Now original at " + i + " is " + original[i]);
		}
	}
}