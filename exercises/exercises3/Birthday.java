package exercises3;

import cse131.ArgsProcessor;

public class Birthday {

	public static void main(String[] args) {
		// N people enter the room
		// do any of them have the same birthday?
		int[][] cal = new int[12][31];

		ArgsProcessor ap = new ArgsProcessor(args);
		int N = ap.nextInt("How many people walked into the room?");

		for (int i = 0; i < N; ++i) {
			// each iteration is a new person walking into the room
			int m = (int) (Math.random() * 12);
			int d = (int) (Math.random() * 31);
			// want to add one to the count of people born on m d
			cal[m][d] = cal[m][d] + 1;
		}

		// check cal to see if there are mult birthdays
		boolean foundMult = false;
		for (int m = 0; m < 12; ++m) {
			for (int d = 0; d < 31; ++d) {
				if (cal[m][d] > 1) {
					foundMult = true;
				}
			}
		}
		System.out.println("Multiple Brithdays? " + foundMult);

	}

}
