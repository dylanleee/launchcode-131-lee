package studio2;

public class Pi {

	public static void computePi() {
		// calculate pi using the 'infinite series'
		// pi = (4/1)-(4/3)+(4/5)-(4/7)+(4/9)-(4/11)+(4/13)-(4/15)...
		// using 10mil. iterations (more repetition==more accurate result)

		double ans = 4.0;
		boolean plus = false;
		for (int i = 3; i < 10000000; i += 2) {
			if (plus) {
				ans += 4.0 / i;
			} else {
				ans -= 4.0 / i;
			}
			plus = !plus;
		}
		System.out.println("I show that Pi = " + ans);
		System.out.print("");
	}

	public static void computePiAgain() {
		// Monte Carlo Dart/Pi Expectation?

		double i;
		double nThrows = 0;
		double nSuccess = 0;

		double x, y;

		for (i = 0; i < 1000000; i++) {
			x = Math.random(); // Throw a dart randomly
			y = Math.random();

			nThrows++;

			if (x * x + y * y <= 1) // distance formula'ish
				nSuccess++;
		}

		System.out.println("Pi/4 = " + nSuccess / nThrows);
		System.out.println("Pi = " + 4 * nSuccess / nThrows);
	}

	public static void computePiClassVersion() {
		double ans = 0.0;
		int darts = 1000000;
		int in = 0;

		// area = pi*r^2

		for (int i = 0; i < darts; i++) {
			double x = Math.random(); // Throw a dart randomly
			double y = Math.random();
			double xdist = (Math.pow((x - .5), 2));
			double ydist = (Math.pow((y - .5), 2));
			double distance = Math.sqrt(xdist + ydist);
			if (distance < 0.5) {
				in++;
			}

			/*
			 * - track ratio of dats hit to total darts - make a random x/y -
			 * middle is (.5,.5) - double distanceF = sqrt((x1-.5)^2+(y1-.5)^2)
			 * - if (distance < 0.5) count the darts in the circle inside++
			 */

		}
		ans = ((double) in / darts) / Math.pow(0.5, 2.0);
		System.out.println("Our group shows that Pi = " + ans);

	}

	public static void main(String[] args) {
		computePi();
		System.out.println("");
		computePiAgain();
		System.out.println("");
		computePiClassVersion();
		System.out.println("");
		System.out.println("Actual Pi: " + Math.PI);

	}
}
