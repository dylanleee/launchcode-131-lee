package studio2;

import cse131.ArgsProcessor;

public class Grid {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int rows = ap.nextInt("How many rows");           //prompt use for amount of rows
		int cols = ap.nextInt("How many columns");        //prompt user for amount of columns
		
			
		for (int i =1;i<=cols;i++){                        //count up to "cols"
			for (int j =0; j<rows;j++){                    //counts over to "rows"
				System.out.print("* ");                    //prints item 
			}
			System.out.println("");                        //line break
		}
	}

}
