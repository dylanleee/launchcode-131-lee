package studio2;

import cse131.ArgsProcessor;

public class GamblersRuin {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); //input popup
		double startMoney = ap.nextDouble("How much money would you like to start off with?");
		double winChance = ap.nextDouble("What would you like the win chance to be?");
		double winAmount = ap.nextDouble("How much money until you 'win'?");
		double sim = ap.nextDouble("How many times would you like to play?");
		
		String wLose="";
              
		double rounds=0;														//initialize rounds
		double loses=0;														//initialize loses
		double win=0;
		double ruin=0;
		
		for (int t=1;t <=sim; t++)  {										//run for user input of simulations
			rounds=0;														
			double curCash = startMoney;
			
			while ((curCash > 0) && (curCash <= winAmount)) {                 //curCash can't be less than 0 and or more than winAmount
				if (Math.random() <= winChance)	{
					curCash= curCash+1;
					
				} else {    
					curCash=curCash-1;
				}
				rounds++;
				
				if (curCash ==0){
					wLose="LOSE";
					loses++;
				}
				else if (curCash>=winAmount){
					wLose="Win";
				}
				

			}
			//win= sim-loses;
			ruin = loses/sim;
			System.out.println("Simulation "+sim+":  "+rounds+" rounds" +"\t"+wLose);
			
		}
		System.out.println("");
		System.out.println("Losses: "+loses+" Simulations: "+sim);
		System.out.println("Ruin Rate: "+ruin);
		//    wins/loses
	}
}
