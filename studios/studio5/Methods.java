package studio5;

public class Methods {

	/**
	 * Implemented correctly
	 * 
	 * @param x
	 *            one thing to add
	 * @param y
	 *            the other thing to add
	 * @return the sum
	 */
	public static int sum(int x, int y) {
		return x + y;
	}

	/**
	 * 
	 * @param x
	 *            one factor
	 * @param y
	 *            another factor
	 * @return the product of the factors
	 */
	public static int mpy(int x, int y) {
		return x * y; // FIXME
	}

	public static double avg3(double x, double y, double z) {
		double num = x + y + z;
		double avg = num / 3;
		return avg;
	}

	public static double sumArray(double[] a) {
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}

	public static double average(double[] a) {
		return (sumArray(a) / a.length);
	}

	public static int yourCall(int x, int y, int z) {
		return x + y + z;
	}

	public static String pig(String a) {
		// first two letters --> last two then add 'ay'
		String x = a.substring(0, 1);
		String y = a.substring(1, a.length());
		String z = "ay";
		return y + x + z;
	}

}
