package studio3;

import cse131.ArgsProcessor;

public class Sieve {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int n = ap.nextInt("Enter the size for the array.");
		int[] sieve = new int[n];
		// populating array with all values 2 through n+1
		for (int r = 0; r < n; r++) {
			sieve[r] = r + 2;
			System.out.print(sieve[r] + " ");
		}

		// iterating over array, checking for anything thats a multiple of a
		// lower number
		// is if assign it zero
		for (int factor = 2; factor < sieve.length / 2; factor++) {
			// only need to run to run up to seive.length/2
			// nothing will be a multiple of that
			// if something true we're not even going to go though
			if (sieve[factor - 2] != 0) {// if sieve[factor] is zero it means
											// than that
				// example 6 would have been factored out by 2 and 3, a previous
				// simpler number
				for (int r = factor - 1; r < sieve.length; r++) {
					// when r r= factor-
					if (sieve[r] % factor == 0) { // if this value is a multiple
													// of factor it's not prime
						sieve[r] = 0;// assign it zero
					}
				}
			}
		}
		// creating new primes array
		int[] primes = new int[sieve.length];
		int index = 0;

		System.out.println(" ");

		for (int i = 0; i < sieve.length; i++) {
			if (sieve[i] != 0) { // if sieve at i = 0, it's prime and we don't
									// care
				primes[index] = sieve[i]; // store the prime value in the primes
											// array
				index++; // incirment the primes in the primes array
				System.out.print(sieve[i] + " ");
			}

		}

		System.out.println(" ");
		for (int i = 0; i < primes.length; ++i) {
			System.out.println("");
			System.out.print(primes[i]);
		}
	}
}
