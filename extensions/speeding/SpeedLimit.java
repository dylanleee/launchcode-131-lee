package speeding;

import cse131.ArgsProcessor;

public class SpeedLimit {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args); //input popup
		int speed = ap.nextInt("How fast were you going?");
		int limit = ap.nextInt("What was this speed limit?");
		
		int difference = speed-limit;
		int fine = (difference > 9) ? 50 +(difference - 10) * 10 : 0;
		
		//final output
		System.out.println("You reported a speed of " +speed+" MPH for a speed limit of " +limit+" MPH. ");
		System.out.println("You went " +difference+ " MPH over the speed limit. Your fine is $"+fine+".");
	}

}
